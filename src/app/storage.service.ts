import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})

export class StorageService {

  constructor(private storage: Storage) { }

  getLevel() {
    return this.storage.get('level');
  }

  getScore() {
    return this.storage.get('score');
  }

  updateLevel(level) {
    this.storage.set('level', level);
  }

  updateScore(score) {
    this.storage.set('score', score);
  }


}
