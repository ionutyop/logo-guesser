import { Component, OnInit } from '@angular/core';
import { LevelService } from './level.service';
import { StorageService } from '../storage.service';
import { level } from './level.model';


@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit {

  level: level = null;
  answer: string;

  constructor(private levelService: LevelService, private storage: StorageService) { }

  ngOnInit() {
    this.storage.getLevel().then(val => {
      this.level = this.levelService.getLevel(val + 1);
      console.log(val);
    });
  }

  verifyAnswer(event) {
    if (event.toLowerCase() === this.level.name) {
      this.storage.updateLevel(this.level.id);
      this.updateLevel();
      this.answer = '';
    }
  }

  updateLevel() {
    this.level = this.levelService.getLevel(this.level.id + 1);
  }



}
