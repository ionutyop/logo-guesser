import { Injectable } from '@angular/core';
import { level } from './level.model';

@Injectable({
  providedIn: 'root'
})
export class LevelService {
  private levels: level[] = [
    {
      id: 1,
      name: "battlenet",
      imageUrl: "../../assets/battlenet.svg"
    },
    {
      id: 2,
      name: "path of exile",
      imageUrl: "../../assets/poe.jpg"
    },
    {
      id: 3,
      name: "half-life",
      imageUrl: "../../assets/half-life.jpg"
    },
    {
      id: 4,
      name: "portal",
      imageUrl: "../../assets/portal.svg"
    },
    {
      id: 5,
      name: "riot",
      imageUrl: "../../assets/riot-games.png"
    },
    {
      id: 6,
      name: "world of warcraft",
      imageUrl: "../../assets/wow.png"
    },
    {
      id: 7,
      name: "assasin creed",
      imageUrl: "../../assets/ac.png"
    },
    {
      id: 8,
      name: "counter strike",
      imageUrl: "../../assets/cs.png"
    },
    {
      id: 9,
      name: "skyrim",
      imageUrl: "../../assets/skyrim.jpg"
    },
    {
      id: 10,
      name: "red alert",
      imageUrl: "../../assets/redalert.png"
    },
    {
      id: 11,
      name: "vvvvvv",
      imageUrl: "../../assets/end.jpg"
    }
  ];

  constructor() { }

  getLevel(levelId: number) {
    return {
      ...this.levels.find(level => {
        return level.id === levelId;
      })
    };
  }

}
