export interface level {
    id: number;
    name: string;
    imageUrl: string;
}