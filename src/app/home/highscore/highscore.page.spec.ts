import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighscorePage } from './highscore.page';

describe('HighscorePage', () => {
  let component: HighscorePage;
  let fixture: ComponentFixture<HighscorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighscorePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighscorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
