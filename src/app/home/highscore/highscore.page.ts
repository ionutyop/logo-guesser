import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../../storage.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Screenshot } from '@ionic-native/screenshot/ngx';


@Component({
  selector: 'app-highscore',
  templateUrl: './highscore.page.html',
  styleUrls: ['./highscore.page.scss'],
})

export class HighscorePage implements OnInit {
  maxScore: number;
  level: number;
  path: string;
  constructor(
    private storage: StorageService,
    private alertController: AlertController,
    private socialSharing: SocialSharing,
    private screenshot: Screenshot) { }

  ngOnInit() {

    this.storage.getLevel().then(level => {
      this.level = level;
    });

    this.storage.getScore().then(score => {
      this.maxScore = score;
      if (!this.maxScore || this.level > this.maxScore) {
        this.maxScore = this.level;
        this.storage.updateScore(this.maxScore);
      }
    });



  }

  shareViaFacebook() {
    // this.socialSharing.share(`I hit level ${this.maxScore} in LogoGuesser`, null , 'src/assets/logo.png' )
    //   .then(() => { })
    // .catch(() => { });
    this.socialSharing.shareViaFacebook(`I hit level ${this.maxScore} in LogoGuesser`, "https://blog.ionicframework.com/wp-content/uploads/2018/11/ionic-logo-white-300x300.png")
      .then(() => { })
      .catch(e => { });
  }

  shareViaTwitter() {
    this.socialSharing.shareViaTwitter(`I hit level ${this.maxScore} in LogoGuesser`, "https://blog.ionicframework.com/wp-content/uploads/2018/11/ionic-logo-white-300x300.png")
      .then(() => { })
      .catch(e => { });
  }

  shareViaEmail() {
    this.socialSharing.shareViaEmail(`I hit level ${this.maxScore} in LogoGuesser`, 'Logo Guesser progress', null,null,null,'https://blog.ionicframework.com/wp-content/uploads/2018/11/ionic-logo-white-300x300.png');
  }

  async resetProgress() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: '<strong>Reset progress?</strong>!!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return;
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.storage.updateLevel(0);
            this.level = 1;
          }
        }
      ]
    });

    await alert.present();
    console.log('lol');
  }

}
